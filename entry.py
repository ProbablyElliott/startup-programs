import datetime
import os
import json

CONFIG_NAME = "config.json"
WEEKDAY = "weekday"
WEEKEND = "weekend"


def is_weekday(val):
    return val < 6


def run_command(exe_loc, args):
    print("{} {}".format(exe_loc, args))


def process_day_requests():
    json_loc = os.path.join(os.path.dirname(os.path.realpath(__file__)), CONFIG_NAME)
    json_content = open(json_loc).read()
    run_data = json.loads(json_content)
    date_val = datetime.datetime.today().weekday()

    if is_weekday(date_val) and WEEKDAY in run_data:
        if len(run_data[WEEKDAY]) > 0:
            for data_key in run_data[WEEKDAY]:
                run_command(data_key, run_data[WEEKDAY][data_key])


if __name__ == "__main__":
    process_day_requests()